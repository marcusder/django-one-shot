from django.urls import path
from todos.views import todoListList, list_details, create

urlpatterns =[
    path('todos/', todoListList, name = "todo_list_list"),
    path('todos/<int:id>/', list_details, name = "todo_list_detail"),
    path('todos/create/', create, name = 'todo_list_create')
]
