from django.shortcuts import render,get_list_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import CreateForm

# Create your views here.

def todoListList (request):
    todolist=TodoList.objects.all()
    context = {
        'list': todolist
    }

    return render(request, 'todos/mylist.html', context)


def list_details(request, id):
    detail= get_list_or_404(TodoItem, list_id=id)
    context = {
        'list_detail' : detail,
    }

    return render(request, 'todos/details.html', context)

def create(request):
    if request.method == "POST":
        form = TodoList(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todoListList')
    else:
        form = TodoList()
    context = {
        'form':form,
    }
    return render(request, 'todos/create.html', context)
